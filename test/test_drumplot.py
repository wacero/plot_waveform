import pytest 
import drumplot
import os 
from obspy.core import UTCDateTime
from Obspy datetime object

def test_create_directory():
    direct_path="./test/data/directorio"
    direct_string_expected = os.makedirs(direct_path)
    direct_string_tested=drumplot.create_direct(direct_path)
    assert os.path.exists(direct_path)

