#!/bin/bash

exec 2> >(logger -s -t $(basename $0))

today=`date -u  +"%Y-%m-%d"`
tomorrow=`date -u --date="next day" +"%Y-%m-%d"`

eval "$(/home/aplicaciones/anaconda3/bin/conda shell.bash hook)"
conda activate plot_waveform

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
cd $DIR

python ./run_drumplot.py ./configuration_production.txt $today $tomorrow 1 1>&2
