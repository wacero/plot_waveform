#!/bin/bash

exec 2> >(logger -s -t $(basename $0))

start_date="2022-12-14"
end_date="2022-12-19"

eval "$(/home/aplicaciones/anaconda3/bin/conda shell.bash hook)"
conda activate plot_waveform

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
cd $DIR

python ./run_drumplot.py ./configuration_fill.txt $start_date $end_date 1 1>&2
