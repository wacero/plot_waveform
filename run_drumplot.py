import drumplot_utils as utils
import drumplot

from obspy.core.utcdatetime import UTCDateTime
from datetime import timedelta,datetime
import os, sys
import logging,logging.config
from multiprocessing import Pool


logging.config.fileConfig('./config/logging.ini', disable_existing_loggers=False)
logger=logging.getLogger('run_drumplot')

def main():

    is_error=False
    
    if len(sys.argv)==1:
        is_error=True
        
        
    else:
        logger.info("Start run_drumplot.py")
        logger.info("Calling arguments %s" %str(sys.argv))
        
        try:
            logger.info("Read configuration file %s" %str(sys.argv[1]))
            run_param=utils.read_parameters(sys.argv[1])
        
        except Exception as e:
            logger.error("Error reading configuration file: %s" %str(e))
            raise Exception("Error reading configuration file: %s" %str(e))
    
        try:
            logger.info("Load parameters")
            mseed_id=run_param['MSEED_SERVER']['id']
            mseed_server_config_file = run_param['MSEED_SERVER']['file_path']
            station_file=run_param['STATION_FILE']['file_path']
            drumplot_dir=run_param['DRUMPLOT']['dir_path']

        except Exception as e:
            logger.critical("Error getting parameters: %s" %str(e))
            raise Exception("Error getting parameters: %s" %str(e))
        
        try:
            mseed_server_param=utils.read_config_file(mseed_server_config_file)
            stations_dict=utils.read_config_file(station_file)
        except Exception as e:
            logger.error("Error reading configuration file: %s" %str(e))
            raise Exception("Error reading configuration file: %s" %str(e))

        try:
            mseed_client=drumplot.choose_service(mseed_server_param[mseed_id])
            
        except Exception as e:
            logger.critical("Failed to create mseed client")
            raise Exception("Failed to create mseed client")

        '''
        Get dates to process
        '''
        try:
            date_start = UTCDateTime(datetime.strptime(sys.argv[2],"%Y-%m-%d"))
            date_end = UTCDateTime(datetime.strptime(sys.argv[3],"%Y-%m-%d"))
            
        except Exception as e:
            logger.critical("Error getting dates: %s" %str(e))
            raise Exception("Error getting dates: %s" %str(e))


        '''
        Choose execution mode
        '''
        try:
            mode=int(sys.argv[4])
        except Exception as e:

            logger.critical("Error getting mode: %s" %str(e))
            raise Exception("Error getting mode: %s" %str(e))

        
        if mode==0:
            
            for day in utils.perdelta(date_start,date_end,timedelta(days=1)):
                day_utc=UTCDateTime(day)
                
                for station_name,station in stations_dict.items():
                    call_plot(mseed_id,mseed_client,station,day_utc,drumplot_dir)
                
        elif mode == 1:
            pool= Pool(processes=10)
            
            for day in utils.perdelta(date_start,date_end,timedelta(days=1)):
                day_utc=UTCDateTime(day)
                
                results=[ pool.apply_async( call_plot,args=(mseed_id,mseed_client,station,day_utc,drumplot_dir),
                                           callback=callback_funtion) for station_name,station in stations_dict.items() ]
                
            for res in results:
                logger.info("Result of multiprocess: %s" %res.get())
            
            pool.close()
            
        else:
            
            logger.info("Wrong mode: %s" %mode)

    if is_error:
        logger.info(f'Usage: python {sys.argv[0]} configuration_file.txt date_start date_end mode')
        print(f'Usage: python {sys.argv[0]} configuration_file.txt date_start date_end mode')    

def callback_funtion(res):
    
    logger.info("Result of multiprocess: %s" %res)

def call_plot(mseed_id,mseed_client,station,day_utc,drumplot_dir):
    
    logger.info("Get data for: %s %s %s" %(station['cod'],station['cha'], day_utc))
    
    stream=drumplot.get_stream(mseed_id,mseed_client,station['net'],station['cod'],station['loc'][0],station['cha'][0],day_utc)
    
    if stream !=None and len(stream)>0:
        drumplot.create_directory(drumplot_dir,station['cod'],day_utc)
        drumplot.save_drumplot(drumplot_dir,stream,day_utc)
    
    else:
        logger.info("No data found for: %s %s %s" %(station['cod'],station['cha'],day_utc))
    
    
if __name__ == "__main__": 
    main()
