This repository consist of code for plotting seismic waveform data stored in MSEED by SeisComP3
## Installation




This repository relies on:

 - Python 3
 - Obspy 1.1
 - Pygelf

Those packages can be installed using `pip`/`conda`

``` bash
$ conda create plot_waveform
$ conda activate plot_waveform
$ conda install obspy pygelf
```

## Install the code
``` bash
$ cd $HOME
$ git https://gitlab.com/wacero/plot_waveform.git

```
To run this code, do

```bash
$ conda activate plot_waveform

$ python3 run_drumplot CONFIGURATION_FILE.txt date_start date_end mode

$ python3 run_drumplot ./configuration_file.txt 2016-04-16 2016-04-17 0

```

## Arquitecture diagram
```mermaid
graph TD
    A[Crontab] -->|Runs| B[run_drumplot.py]
    B -->|Reads Configuration| C[Config Files]
    B -->|Uses| D[drumplot_utils.py]
    B -->|Invokes| E[drumplot.py]
    E -->|Processes Data| F[Obspy Library]
    E -->|Logging| G[Pygelf Library]
    E -->|Generates| H[Plot Output]

```


## Component diagram
```mermaid
classDiagram
    class run_drumplot {
        +main()
    }
    class drumplot {
        +plot_waveform()
    }
    class drumplot_utils {
        +utility_functions()
    }
    class Config_Files {
        +configuration_parameters()
    }
    class Obspy_Library {
        +data_processing()
    }
    class Pygelf_Library {
        +logging()
    }

    run_drumplot --|> drumplot : invokes
    run_drumplot --|> drumplot_utils : uses
    run_drumplot --|> Config_Files : reads
    drumplot --|> Obspy_Library : processes data with
    drumplot --|> Pygelf_Library : logs with
    drumplot --|> drumplot_utils : uses

```

## LR diagram
```mermaid
graph LR
    A[User or Crontab] -->|Runs| B[run_drumplot.py]
    B -->|Reads Configuration| C[Config Files]
    B -->|Uses| D[drumplot_utils.py]
    B -->|Invokes| E[drumplot.py]
    E -->|Processes Data| F[Obspy Library]
    E -->|Logging| G[Pygelf Library]
    E -->|Generates| H[Plot Output]

```


## Documentation

Please check the documentation of the project at https://wacero.gitlab.io/plot_waveform/
