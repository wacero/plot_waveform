'''
This module contains methods and functions to 
get MSEED data and plot it as a PNG image file  
'''

import os 

from obspy.clients.arclink import Client as clientArclink
from obspy.clients.seedlink import Client as clientSeedlink
from obspy.clients.filesystem.sds import Client as clientArchive
from obspy.clients.fdsn import Client
import numpy as np
import logging

logger=logging.getLogger('run_drumplot')

       


def choose_service(server_parameter_dict):
    '''
    Funtion that connects to a mseed data server (ARCLINK|SEEDLINK|ARCHIVE) and returns a client
    
    :type server_parameter_dict: dict
    :param server_parameter_dict: dictionary with the parameters of the MSEED server
    
    :return: obspy.client object    
    '''
    logger.info("start choose_service")
    if server_parameter_dict['name'] == 'ARCLINK':
        try:
            logger.info("Trying  Arclink : %s %s %s" %(server_parameter_dict['user'],server_parameter_dict['server_ip'],server_parameter_dict['port']) )
            return clientArclink(server_parameter_dict['user'],server_parameter_dict['server_ip'],int(server_parameter_dict['port']))
        except Exception as e:
            logger.fatal("Error Arclink : %s -- %s %s %s" %(str(e), server_parameter_dict['user'],server_parameter_dict['server_ip'],server_parameter_dict['port']) )
            raise Exception("Error Arclink : %s -- %s %s %s" %(str(e), server_parameter_dict['user'],server_parameter_dict['server_ip'],server_parameter_dict['port']) )
            
    elif server_parameter_dict['name'] == 'SEEDLINK':
        try:
            logger.info("Trying  Seedlink : %s %s" %(server_parameter_dict['server_ip'],server_parameter_dict['port']) )
            return clientSeedlink(server_parameter_dict['server_ip'],int(server_parameter_dict['port']),timeout=5 )
        except Exception as e:
            logger.fatal("Error Seedlink :%s -- %s %s" %(str(e),server_parameter_dict['server_ip'],server_parameter_dict['port']) )
            raise Exception("Error Seedlink :%s -- %s %s" %(str(e),server_parameter_dict['server_ip'],server_parameter_dict['port']) )
            
    elif server_parameter_dict['name'] == 'ARCHIVE':
        try:
            logger.info("Trying  Archive : %s " %(server_parameter_dict['archive_path']) )
            return clientArchive(server_parameter_dict['archive_path'])
        except Exception as e:
            logger.fatal("Error Archive : %s -- %s" %(str(e),server_parameter_dict['archive_path']))
            raise Exception("Error Archive : %s -- %s" %(str(e),server_parameter_dict['archive_path']))



def get_stream(service,conSrv,net,station_code,loc,cha,day_utc):
    '''
    This function ask for data to a mseed data server and returns a mseed stream 
    The parameter of the stream are called explicitly to enhace the readability of the code. 
    
    :type service: string
    :parameter service: name of the service to use
    :type conSrv: obspy.client
    :parameter conSrv: Obspy client object to connect to a data server
    :type net: string
    :parameter net: Network of the data, e.g. EC, CO, GE
    :type station_code: string
    :parameter station_code: Name of the station, e.g. BMAS, ZUMB
    :type loc: string
    :parameter loc: Location of the station, usually this is an empty value '' or 00, 01, etc.
    :type cha: string
    :parameter cha: Channel of the station, e.g. BHZ, HHZ, BLZ
    :type day_utc: obspy.UTCDateTime
    :parameter day_utc: obspy datetime object. Indicates the start datetime of the requested data   
    :return: obspy stream object. 
    '''
    
    logger.info('Starting station: %s %s %s %s' %(net,station_code,loc,cha))
    if service=='ARCLINK':
        try:
            return conSrv.get_waveforms(net,station_code,loc,cha,day_utc+0.01,day_utc + 86400,route=False,compressed=False)
        except Exception as e:
            logger.fatal("Error getting stream_station: %s" %str(e))
    else:
        try:
            return conSrv.get_waveforms(net,station_code,loc,cha,day_utc+0.01,day_utc + 86400)
        except Exception as e:
            logger.fatal("Error getting stream_station: %s" %str(e))


def create_directory(drumplot_dir,station_code,day_utc):
    '''
    Create a directory in the drumplot directory for each year/station_station_code
    
    :type station_code: string
    :parameter station_code: name of the station
    
    :type day_utc: obspy.UTCDateTime
    :parameter day_utc: Obspy datetime object 
    '''

    direct="%s/%s/%s" %(drumplot_dir,day_utc.year,station_code)
    if not os.path.exists(direct):
        logger.info("creating folder: %s" %(direct))
        os.makedirs(direct)

def connect_fdsn(servidor_fdsn,port):
    """
    Connect to a FDSNWS server
    
    :param string servidor_fdsn: Hostname or IP of the FDSN server
    :param int port: Port number to connect to. 
    :returns obspy.clients.fdsn 
    :raises Exception e: Log if the connection couldn't be stablished and exit. 
    """
    
    logging.info("Connect to FDSNWS")
    try:
        client=Client("http://%s:%s" %(servidor_fdsn,port))
        return client
    except Exception as e:
        logging.fatal("Error while connecting to FDSN: %s,%s. Error was: %s" %(servidor_fdsn,port,str(e)))
        raise Exception("Error while connecting to FDSN: %s,%s. Error was: %s" %(servidor_fdsn,port,str(e)))

def get_station_inventory(fdsn_client,trace ):
    """
    Add coordinates info got from FDSN server as an atribDict in a trace object.  
    
    :param obspy.fdsn.client fdsn_client: client to connect to a FDSN server
    :param obspy.trace trace: waveform data
    :returns: obspy.trace with latitude, longitude and elevation attached as coordinates dict. 
    :raises Exception e: Log if fails to get data  
    """
    try:
        station_inventory=fdsn_client.get_stations(network=trace.stats.network,station=trace.stats.station,channel=trace.stats.channel,level="response",format=None)       
        #trace.stats.coordinates=AttribDict({'latitude':station_info[0][0][0].latitude,'longitude':station_info[0][0][0].longitude,'elevation':station_info[0][0][0].elevation})
        #logger.info(type(station_inventory))
        return station_inventory
    
    except Exception as e:
        logging.info("Fail to get data for %s. Error was: %s" %(trace,str(e)))


def process_stream(stream):
    
    if len(stream) >1:
        sampling_list=[]
        for trace in stream:
            sampling_list.append(trace.stats['sampling_rate'])
        
        sampling_set=set(sampling_list)
        
        if len(sampling_set) >1:
        
            logger.warning("WARNING: Multiple sampling rates in stream: %s" %stream)
            stream=stream.sort(keys=['npts'])
            prefered_sampling_rate=stream[-1].stats['sampling_rate']    
        
            for trace in stream:
                
                if trace.stats['sampling_rate'] != prefered_sampling_rate:
                    stream.traces.remove(trace)
        
        #sampling_rate=round(st.stats['sampling_rate'],0)
        #st.resample(sampling_rate)
        #st.data=st.data.astype(np.int32)

    stream.merge(method=1,fill_value="interpolate",interpolation_samples=0)
    return stream



def save_drumplot(drumplot_dir,stream,day_utc):
    '''
    This function calls stream.plot() and stores it as a PNG image in the drumplot_dir
    
    :type stream: obspy stream object
    :parameter stream: station object with a plot method.
    :type day_utc: obspy.UTCDateTime
    :parameter day_utc: Obspy datetime object 
    '''
    try:
        stream=process_stream(stream)
    except Exception as e:
        logger.error("Couldn't process_stream: %s" %str(e))
    
    
            
    image_name="%s-%s-%s-%s" %(stream[0].stats['station'],stream[0].stats['channel'],stream[0].stats['network'],day_utc.strftime("%Y-%m-%d"))
    #image_name="%s.%s.%s.%s" %(stream[0].stats['network'],stream[0].stats['station'],stream[0].stats['channel'],day_utc.strftime("%Y.%m.%d"))

    plot_dir="%s/%s/%s" %(drumplot_dir,day_utc.year,stream[0].stats['station'])
    
    try:
        ##REALIZAR UN MERGE DE LAS FORMAS DE ONDA 
        ##PLOTEAR TODA LA FORMA DE ONDA NO SOLO stream[0]

        #logger.info("station: %s, max_value:%s" %(stream[0].stats['station'],stream[0].max()))
        #fdsn_cl=connect_fdsn(servidor_fdsn,port)
        #station_inventory=get_station_inventory(fdsn_cl, stream[0])
        #stream[0].remove_response(inventory=station_inventory)
    
        if stream[0].stats['station']== "SAGA" and stream[0].stats['channel']=="BDF":
            stream[0].filter("highpass",freq=0.05)
        
        stream.plot(type='dayplot', interval=60, color='k',number_of_ticks=7,\
                 linewidth=0.25,grid_linewidth=0,one_tick_per_line=True,  \
                 show_y_UTC_label=True, size=(1400,900),dpi=130,title_size=10,\
                 y_label_size=6,outfile='%s/%s.jpg'%(plot_dir,image_name),tick_format="%H",\
                 title=image_name)
        
        logger.info("Created plot: %s" %(image_name))
    except Exception as e:
        logger.info("Error in save_drumplot(): %s" %(str(e)))


